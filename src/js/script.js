

document.addEventListener("DOMContentLoaded", function () {


    var connexion = new MovieDB();


    if (location.pathname.search("fiche-film.html") > 0){

        var parametres = new URL(document.location).searchParams;

        //console.log(parametres.get("id"));

        connexion.requeteInfoFilm(parametres.get("id"));
        connexion.afficheInfoFicheFilm(parametres.get("id"))

    } else{

        connexion.requeteFilmPopulaire();
        connexion.requeteFilmAffic();

    }

    var intervalId = 0; // Needed to cancel the scrolling when we're at the top of the page
    var scrollButton = document.getElementById('scroll'); // Reference to our scroll button

    function scrollStep() {
        // Check if we're at the top already. If so, stop scrolling by clearing the interval
        if (window.pageYOffset === 0) {
            clearInterval(intervalId);
        }
        window.scroll(0, window.pageYOffset - 50);
    }

    function scrollToTop() {
        // Call the function scrollStep() every 16.66 millisecons
        intervalId = setInterval(scrollStep, 16.66);
    }

// When the DOM is loaded, this click handler is added to our scroll button
    scrollButton.addEventListener('click', scrollToTop);


    // var swiper = new Swiper('.swiper-container', {
    //     slidesPerView: 3,
    //     spaceBetween: 30,
    //     slidesPerGroup: 3,
    //     loop: true,
    //     pagination: {
    //         el: '.swiper-pagination',
    //     },
    //     loopFillGroupWithBlank: true,
    //     navigation: {
    //         nextEl: '.swiper-button-next',
    //         prevEl: '.swiper-button-prev',
    //     },
    // });


});


class MovieDB{

    constructor(){
        console.log("Parfait 2");

        this.APIKey = "eda01ad95b124c2be1b5f4308d87648f";

        this.lang = "fr-CA";

        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "https://image.tmdb.org/t/p/";

        this.largeurAffiche = ["92", "154", "185", "342", "500", "780"];

        this.largeurTeteAffiche = ["45", "185"];

        this.totalFilm = 8;

        this.totalFilmAff = 9;

        this.totalActeur = 6;
    }

    requeteFilmPopulaire(){


        var xhr = new XMLHttpRequest();


        xhr.addEventListener("readystatechange", this.retourFilmPopulaire.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/popular?page=1&language=en-US&api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();
    }

    requeteFilmAffic(){


    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", this.retourFilmAffic.bind(this));

    xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIKey);

    xhr.send();

}




    retourFilmPopulaire(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheFilmPop(data);

        }

    }

    retourFilmAffic(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).results;

            this.afficheFilmSwiper(data);

        }

    }



    afficheFilmPop(data){

        console.log( data );

        for (var i = 0; i < this.totalFilm; i++) {

            var unArticle = document.querySelector(".template>.film").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            if (data[i].overview === "") {

                unArticle.querySelector(".description").innerText = "pas de description";

            }
            else {

                unArticle.querySelector(".description").innerText = data[i].overview;

            }

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
            unArticle.querySelector("img").setAttribute("alt", data[i].title);
            unArticle.setAttribute("href", "fiche-film.html?id=" + data[i].id);


            document.querySelector(".liste-films").appendChild(unArticle);
        }
}

    afficheFilmSwiper(data){

        console.log( data );

        for (var i = 0; i < this.totalFilmAff; i++) {

            var unArticle = document.querySelector(".template>.swiper-slide").cloneNode(true);

            unArticle.querySelector("h1").innerText = data[i].title;

            unArticle.querySelector("img").setAttribute("src", this.imgPath + "w500" + data[i].poster_path);
            unArticle.querySelector("img").setAttribute("alt", data[i].title);
            unArticle.setAttribute("href", "fiche-film.html?id=" + data[i].id);
            unArticle.querySelector("p.noteSwipe").innerText = data[i].vote_average;

            document.querySelector(".swiper-wrapper").appendChild(unArticle);
        }

        var swiper = new Swiper('.swiper-container', {
            slidesPerView: 3,
            spaceBetween: 30,
            slidesPerGroup: 3,
            loop: true,
            pagination: {
                el: '.swiper-pagination',
            },
            loopFillGroupWithBlank: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    }

// afficher un film

    requeteInfoFilm(movieId){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfoFilm.bind(this) );

        xhr.open("GET", this.baseURL + "movie/" + movieId + "?language=" + this.lang + "&api_key=" + this.APIKey);

        xhr.send();

    }

    retourRequeteInfoFilm(e){

        var target = e.currentTarget;

        var data;

        if(target.readyState === target.DONE){

            data = JSON.parse(target.responseText);

            this.afficheInfoFicheFilm(data);
            this.afficheInfoFilm(data);

            console.log(data);

        }

    }

    afficheInfoFicheFilm(data){


        document.querySelector(".lg").innerText = data.original_language;

        document.querySelector(".dt").innerText = data.release_date;

        document.querySelector(".dr").innerText = data.runtime + " minutes";

        document.querySelector(".bg").innerText = data.budget + " $";

        document.querySelector(".rv").innerText = data.revenue + " $";

        document.querySelector(".rt").innerText = data.vote_average;





    }

    afficheInfoFilm(data){

        console.log( data );

        document.querySelector(".fiche-film>.carouBackGround>.infos>h1").innerText = data.title;

        if (data.overview === "") {

            document.querySelector(".description").innerText = "pas de description";

        }
        else {

            document.querySelector(".description").innerText = data.overview;

        }

        document.querySelector(".fiche-film>.carouBackGround>img").setAttribute("src", this.imgPath + "w780" + data.poster_path);
        document.querySelector(".fiche-film>.carouBackGround>img").setAttribute("alt", data.title);

        this.requeteActeur(data.id);

    }

    requeteActeur(movieId){

        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourInfoActeur.bind(this));

        //xhr.open("GET", "https://api.themoviedb.org/3/movie/%7Bmovie_id%7D/credits?api_key=%3C%3Capi_key%3E%3E");
        xhr.open("GET", this.baseURL + "movie/" + movieId + "/credits?api_key="+this.APIKey);

        xhr.send();

    }

    retourInfoActeur(e){

        var target = e.currentTarget;

        var data;

        if (target.readyState === target.DONE) {

            data = JSON.parse(target.responseText).cast;

            this.afficheActeur(data);

        }

    }

    afficheActeur(data){

        console.log( data );

        for (var i = 0; i < this.totalActeur; i++) {

            var unActeur = document.querySelector(".template>.acteur").cloneNode(true);

            unActeur.querySelector("h1").innerText = data[i].name;

            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w" + this.largeurAffiche[4] + data[i].profile_path);
            unActeur.querySelector("img").setAttribute("alt", data[i].name);

            document.querySelector(".liste-acteurs").appendChild(unActeur);

        }
    }


}